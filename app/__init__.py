from flask import Flask, render_template
from flask_bootstrap import Bootstrap
#import eventlet
#eventlet.monkey_patch()

from app import config
from app import model

# init model before app, so that mqttclient startup can populate it
model.init_model( config.NUM_ROOMS )

# init webapp
app = Flask(__name__)

# moved app config to its own file
## see https://flask.palletsprojects.com/en/1.1.x/config/
app.config.from_object( config )

# setup routes
from app import routes, errors

# setup flask-bootstrap extension
bootstrap = Bootstrap(app)



