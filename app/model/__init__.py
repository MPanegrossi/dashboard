
from enum import Enum

room_names = {1:"Mexico City",2:"Tokyo",3:"Paris",4:"Rio de Janeiro",5:"Barcelona",6:"San Francisco",7:"London",8:"Seoul",9:"Lagos",10:"Cape Town",11:"Berlin",12:"Sydney",13:"New York City",14:"New Delhi",15:"Cairo",16:"Toronto", 17:"Not in use", 18:"Not in use", 19:"Not in use", 20:"Not in use", 21:"Not in use", 22:"Not in use", 23:"Not in use", 24:"Not in use", 25:"Not in use", 26:"Not in use", 27:"Not in use", 28:"Not in use", 29:"Not in use", 30:"Not in use"}

class RoomState( Enum ):
    UNKNOWN = 4
    EMPTY = 0
    IN_USE = 1
    FINAL_MINUTES = 2
    CLEANING = 3


class Room( object ):

    __instances = []

    def __init__( self, room_number, room_name, state=RoomState.UNKNOWN ):
            self.room_number = room_number
            self.state = state
            self.room_name = room_name
            Room.add_instance( self )
    
    
    @classmethod
    def add_instance( cls, instance ):
        Room.__instances.append( instance )
    

    """
    Returns a list of all Room objects in the model.
    """
    @classmethod
    def instances( cls ):
        return Room.__instances


"""
Fill the model with blank rooms, ready to have their state set.
"""
def init_model( num_rooms ):
    for i in range( num_rooms ):
        r = Room( i, room_names[i+1])


"""
Convenience method, set the state of the room specified by room_number (0-indexed).
"""
def set_state_for_room( room_number, state ):
    
    # get master list of Rooms
    instances = Room.instances()
    
    # this block is here to check if a set_state comes in for a room numbered higher than we're expecting
    # if that happens, populate the Rooms list up to the number of the specified room
    l = len( instances )
    if room_number > l:
        delta = room_number - l
        for i in range( delta ):
            r = Room( i + l )
    
    # if state isn't a RoomState enum, make it one
    if type(state) != RoomState:
        state = RoomState( state )

    # set the state of the corresponding Room object in the master list of Rooms.
    this_room = instances[ room_number ]
    this_room.state = state
    
"""
Convenience method, get the state of the room specified by room_number (0-indexed).
"""
def get_state_for_room( room_number ):
    instances = Room.instances()
    this_room = instances[ room_number ]
    return( this_room.state )

