import os 

SECRET = 'my secret key'

# TEMPLATES_AUTO_RELOAD defaults to no, but can be overriden with an environment var
TEMPLATES_AUTO_RELOAD = os.environ.get( "TEMPLATES_AUTO_RELOAD", "no" ) == "yes"

# Parameters for SSL enabled
# app.config['MQTT_BROKER_PORT'] = 8883
# app.config['MQTT_TLS_ENABLED'] = True
# app.config['MQTT_TLS_INSECURE'] = True
# app.config['MQTT_TLS_CA_CERTS'] = 'ca.crt'

# MQTT_ENABLED defaults to yes, but can be overriden with an environment var
MQTT_ENABLED = os.environ.get( "MQTT_ENABLED", "yes" ) == "yes"
if MQTT_ENABLED:
    # MQTT_BROKER_URL = '192.168.0.10'
    MQTT_BROKER_URL = 'localhost'
    MQTT_BROKER_PORT = 1883
    MQTT_USERNAME = ''
    MQTT_PASSWORD = ''
    MQTT_KEEPALIVE = 5
    MQTT_TLS_ENABLED = False

NUM_ROOMS = 30
NUM_ROOM_FIRST_FLOOR = 10
NUM_STATES = 4

BOOTSTRAP_SERVE_LOCAL = True
