import json
import random

from flask import render_template, redirect, url_for, request, jsonify
from flask_mqtt import Mqtt
from flask_socketio import SocketIO

from app import model
from app import app


# set up mqtt & socketiọ
mqtt = None
if app.config[ "MQTT_ENABLED" ]:
    mqtt = Mqtt(app)
socketio = SocketIO(app)

# popluate some fake data for now
def populate_model():
    rooms = model.Room.instances()
    possible_states = list( model.RoomState )
    for room in rooms:
        room.state = possible_states[0]
populate_model()


##
# URL HANDLERS
#


"""
Main room listing generator. Used by all routes.
"""
def _room_listing( min_range=0, max_range=None, controls_enabled=False, title=None ):
    
    max_range = app.config[ "NUM_ROOMS" ] if max_range is None else max_range
    # get a slice of the rooms list within the range specified
    rooms = model.Room.instances()[ min_range : max_range ]

    return render_template(
        'listing_base.html',
        title = title,
        rooms = rooms,
        controls_enabled = controls_enabled,
        first_floor = app.config["NUM_ROOM_FIRST_FLOOR"]
    )


"""
View state of all rooms.
"""
@app.route('/')
@app.route('/index')
@app.route('/view')
def index():
    return _room_listing(
        title='View only'
    )


"""
View rooms in a numbered range, e.g
/rooms/1/20 
to show the state of rooms 1-20
"""
@app.route('/rooms/<min_range>/<max_range>')
def room_range( min_range, max_range ):
    # min_range and max_range are human-readable (1-indexed) 
    # return a list of rooms within the range specified in the url
    return _room_listing(
        min_range = int(min_range)-1,
        max_range = int(max_range),
        title='View only'
    )


"""
Control state of all rooms.
"""
@app.route('/control')
def control():
    return _room_listing(
        title = 'Control view',
        controls_enabled = True
    )


"""
Control state of rooms in a numbered range, e.g
/control/rooms/1/20 
to control the state of rooms 1-20
"""
@app.route('/control/rooms/<min_range>/<max_range>')
def control_range( min_range, max_range ):
    # min_range and max_range are human-readable (1-indexed) 
    # return a list of rooms within the range specified in the url
    return _room_listing(
        title = 'Control view',
        controls_enabled = True,
        min_range = int(min_range)-1,
        max_range = int(max_range)
    )


"""
This route exposes an API for state data.
"""
@app.route( '/api/v1/states', methods=['GET', 'POST'])
def api_states():

    # POST requests are made by state button clicks in the browser (ajax)
    if request.method == 'POST':
        # room = request.form[ "room" ]
        # state = request.form[ "state" ]
        print( "State button pressed in browser:", request.form )
        if mqtt:

            # construct the outgoing payload just by taking the incoming one and casting everything to ints
            # passing through the recieved form data directly like this is probably quite lazy / brittle
            # but will only break if we change the data the browser is sending! 

            # construct outgoing payload
            payload = {}
            for key in request.form:
                # cast string values to ints
                payload[ key ] = int( request.form[ key ] )

            # send outgoing payload
            mqtt.publish(
                "dashboard",
                json.dumps( payload )
            )
        return "OK"

    # GET requests are only used for debugging for now
    elif request.method == 'GET':
        instances = model.Room.instances() 
        out = []
        for room in instances:
            out.append({
                "room" : room.room_number,
                "state" : room.state.value
            })
        return jsonify( out )


"""
This route exposes an API for setting brightness.
"""
@app.route( '/api/v1/brightness', methods=['POST'])
def api_brightness():
    # POST requests are made by state button clicks in the browser (ajax)
    if request.method == 'POST':
        if mqtt:
            mqtt.publish(
                "dashboard",
                json.dumps({
                    "brightness" : int(request.form[ "value" ]), "room": 100
                })
            )
        return "OK"


##
# MQTT HANDLING
#

if mqtt is not None:

    @mqtt.on_connect()
    def handle_connect(client, userdata, flags, rc):
            mqtt.subscribe('rooms/#')

    @mqtt.on_message()
    def handle_mqtt_message(client, userdata, message):
        
        # parse json-formatted payload
        payload = json.loads( message.payload.decode() )

        # topic will be e.g "rooms/12"
        # split it into a list that looks like e.g [ "rooms", "12" ] 
        topic_path = message.topic.split( "/" )

        if topic_path[0] == 'rooms':
            
            # get room number from topic
            room = int(topic_path[ 1 ])

            # get state from payload
            state = payload['state']

            # log something useful...
            print("Room # {0} is in state {1}".format(room, state))
            
            # store state in model
            model.set_state_for_room( room, state )

            # forward message to browser over socketio
            payload[ "room" ] = room
            socketio.emit(
                "rooms",
                payload
            )

