# see https://flask-socketio.readthedocs.io/en/latest/#gunicorn-web-server
gunicorn --bind 0.0.0.0:8000 --worker-class eventlet -w 1 app:app
