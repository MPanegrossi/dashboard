dashboard

# Installation

1. Setup a virtual environment in the root of this repository: `python3 -m venv .venv`
2. Activate the virtual environment: `source .venv/bin/activate`
3. Install python dependencies with pip: `pip install -r requirements.txt`