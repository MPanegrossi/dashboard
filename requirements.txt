astroid==2.3.3
Click==7.0
dnspython==1.16.0
dominate==2.4.0
eventlet==0.25.1
Flask==1.1.1
Flask-Bootstrap==3.3.7.1
Flask-MQTT==1.0.5
Flask-SocketIO==4.2.1
Flask-WTF==0.14.2
greenlet==0.4.15
gunicorn==20.0.4
isort==4.3.21
itsdangerous==1.1.0
Jinja2==2.10.3
lazy-object-proxy==1.4.3
MarkupSafe==1.1.1
mccabe==0.6.1
monotonic==1.5
paho-mqtt==1.5.0
pylint==2.4.4
python-engineio==3.11.2
python-socketio==4.4.0
six==1.13.0
typed-ast==1.4.0
typing==3.7.4.1
visitor==0.1.3
Werkzeug==0.16.0
wrapt==1.11.2
WTForms==2.2.1
